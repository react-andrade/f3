import React from 'react';
import ReactDOM from 'react-dom/client';

const element = (
  <nav>
    <h1 className="brand-name">Cisco</h1>
    <ul>
      <li>Pricing</li>
      <li>About</li>
      <li>Contact</li>
    </ul>
  </nav>
)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(element);

